import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  isValidFormSubmitted: boolean;

  constructor(private fb: FormBuilder) { }

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,12}$";

  ngOnInit() {
    this.isValidFormSubmitted = false;
    this.loginForm = this.fb.group({  // Crée une instance de FormGroup
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],                      // Crée une instance de FormControl                    // Crée une instance de FormControl
      password: ['', [Validators.required, Validators.pattern(this.pwdPattern)]],                   // Crée une instance de FormControl
    });

    this.loginForm.get('email').setValidators(Validators.email);
    this.loginForm.get('password').setValidators(Validators.required);
  }

  login() {
    this.isValidFormSubmitted = false;
    if (this.loginForm.invalid) {
      return;
    }
    this.isValidFormSubmitted = true;
    console.log('Données du formulaire...', this.loginForm);
  }

  get primaryEmail() {
    return this.loginForm.get('email');
  } 
}
